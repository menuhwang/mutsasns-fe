import axios from "axios";

async function axiosPost({url, data, token}) {
    const headers = token === undefined ? {
        'Content-Type': 'application/json'
    } : {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
    };

    return await axios({
        method: 'POST',
        url,
        data,
        headers
    });
}

export default axiosPost;