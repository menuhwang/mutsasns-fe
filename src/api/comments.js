import axiosGet from "./axiosGet";
import {API_BASE_URL, ENDPOINT} from "../constant";
import axiosPost from "./axiosPost";

export async function getComments(id) {
    return await axiosGet({
        url: API_BASE_URL + ENDPOINT.POST + `/${id}/comments`
    })
}

export async function writeComment(postId, comment, token) {
    const url = API_BASE_URL + ENDPOINT.POST + `/${postId}/comments`;
    const data = {comment};
    return await axiosPost({url, data, token});
}