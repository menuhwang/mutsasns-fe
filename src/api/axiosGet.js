import axios from "axios";

async function axiosGet({url, params, token}) {
    const headers = token === undefined ? {
        'Content-Type': 'application/json'
    } : {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
    };

    return await axios({
        method: 'GET',
        url,
        params,
        headers
    });
}

export default axiosGet;