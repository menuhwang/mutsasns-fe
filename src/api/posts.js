import axiosGet from "./axiosGet";
import {API_BASE_URL, ENDPOINT} from "../constant";
import axiosPost from "./axiosPost";

export async function getPosts(page = 0, size = 20) {
    return await axiosGet({
        url: API_BASE_URL + ENDPOINT.POST,
        params: {page, size}
    });
}

export async function getPost(id) {
    return await axiosGet({
        url: API_BASE_URL + ENDPOINT.POST + `/${id}`,
    })
}

export async function writePost(title, body, token) {
    return await axiosPost({
        url: API_BASE_URL + ENDPOINT.POST,
        data: {title, body},
        token
    })
}

export async function likes(id, token) {
    return await axiosPost({
        url: `${API_BASE_URL+ENDPOINT.POST}/${id}/likes`,
        token
    })
}