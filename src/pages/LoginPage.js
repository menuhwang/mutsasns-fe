import {Button, Card, Form} from "react-bootstrap";
import {login} from "../api/auth";
import {useState} from "react";
import {useRecoilState} from "recoil";
import {tokenState} from "../recoil";

function LoginPage() {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [token, setToken] = useRecoilState(tokenState);

    return (
        <>
            <Card>
                <Form>
                    <Card.Header><h3>Login</h3></Card.Header>
                    <Card.Body>
                        <Form.Group className="mb-3" controlId="formUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Enter username" onChange={(e) => {setUsername(e.target.value)}}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" onChange={(e) => {setPassword(e.target.value)}}/>
                        </Form.Group>
                        <Button variant="warning" onClick={() =>
                            login(username, password)
                                .then((res) => {
                                    const jwt = res.data.result.jwt;
                                    if (jwt){
                                        setToken(jwt);
                                        window.location.href = "/";
                                    }
                                })
                                .catch((err) => alert(err.response.data.result.message))
                        }>
                            Login
                        </Button>
                    </Card.Body>
                </Form>
            </Card>
        </>
    );
}

export default LoginPage;