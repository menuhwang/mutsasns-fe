import {BrowserRouter, Route, Routes} from "react-router-dom";
import HomePage from "./HomePage";
import LoginPage from "./LoginPage";
import JoinPage from "./JoinPage";
import BoardNew from "../components/board/BoardNew";

function Pages() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<HomePage/>}/>
                <Route path="/login" element={<LoginPage/>}/>
                <Route path="/join" element={<JoinPage/>}/>
                <Route path="/boards/new" element={<BoardNew/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default Pages;