import {useContext, useEffect, useState} from "react";
import {getPosts} from "../api/posts";
import Board from "../components/board/Board";
import BoardNew from "../components/board/BoardNew";
import PaginationBar from "../components/board/PaginationBar";
import {useSearchParams} from "react-router-dom";
import CommentModalContextProvider from "../components/contexts/CommentModalContextProvider";
import CommentsModal from "../components/board/comments/CommentsModal";
import {LoginStatusContext} from "../components/contexts/UserLoginContextProvider";

function HomePage() {
    const {isLoggedIn} = useContext(LoginStatusContext);
    const [paging, setPaging] = useState({});
    const [boardList, setBoardList] = useState([]);
    const [searchParams] = useSearchParams();

    const page = searchParams.get('page');

    useEffect(() => {
        if (boardList.length === 0) {
            getPosts(page).then((res) => {
                setPaging({
                    totalPages : res.data.result.totalPages,
                    currentPage : res.data.result.number
                });
                setBoardList(res.data.result.content);
            }).catch((err) => alert(err.data.result.message))
        }
    }, [])

    return (
        <>
            {isLoggedIn ? <BoardNew/> : <></>}
            <CommentModalContextProvider>
                {boardList.map((board) => <Board key={board.id} data={board}/>)}
                <CommentsModal/>
            </CommentModalContextProvider>
            <PaginationBar currentPage={paging.currentPage} lastPage={paging.totalPages}/>
        </>
    );
}

export default HomePage;