import './App.css';
import NavigationBar from "./components/NavigationBar";
import {Container} from "react-bootstrap";
import Pages from "./pages/Pages";
import Footer from "./components/Footer";
import {RecoilRoot} from "recoil";
import UserLoginContextProvider from "./components/contexts/UserLoginContextProvider";
function App() {
    return (
        <>
            <RecoilRoot>
                <UserLoginContextProvider>
                    <NavigationBar/>
                    <Container className="content-wrapper my-5 px-lg-5">
                        <Pages/>
                    </Container>
                </UserLoginContextProvider>
                <Footer/>
            </RecoilRoot>
        </>
    );
}

export default App;
