function Footer() {
    return (
        <div className="footer">
            <hr/>
            <strong className="ms-2">@made by MenuHwang</strong>
            <a className="ms-2" href="https://github.com/menuhwang/MutsaSNS">GitHub</a>
        </div>
    );
}

export default Footer;