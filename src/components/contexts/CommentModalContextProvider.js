import {createContext, useState} from "react";

export const SelectedBoardContext = createContext({});
export const CommentDataContext = createContext({});
export const ModalsStateContext = createContext(false);
export const ModalsHandlerContext = createContext({})
function CommentModalContextProvider({children}) {
    const [isOpened, setOpened] = useState(false);
    const [selectedId, setSelectedId] = useState();
    const [comments, setComments] = useState([]);

    function handleShow() {
        setOpened(true);
    }

    function handleClose() {
        setOpened(false);
    }

    const board = {selectedId, setSelectedId};
    const handler = {handleShow, handleClose};
    const commentData = {comments, setComments};

    return (
        <SelectedBoardContext.Provider value={board}>
            <CommentDataContext.Provider value={commentData}>
                <ModalsHandlerContext.Provider value={handler}>
                    <ModalsStateContext.Provider value={isOpened}>
                        {children}
                    </ModalsStateContext.Provider>
                </ModalsHandlerContext.Provider>
            </CommentDataContext.Provider>
        </SelectedBoardContext.Provider>
    );
}

export default CommentModalContextProvider;