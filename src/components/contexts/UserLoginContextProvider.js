import {createContext, useState} from "react";

export const LoginStatusContext = createContext({});
export const LoginUserContext = createContext({});

function UserLoginContextProvider({children}) {
    const [isLoggedIn, setLoggedIn] = useState(false);
    const [userData, setUserData] = useState({});

    const loggedIn = {isLoggedIn, setLoggedIn};
    const user = {userData, setUserData};

    return (
        <LoginStatusContext.Provider value={loggedIn}>
            <LoginUserContext.Provider value={user}>
                {children}
            </LoginUserContext.Provider>
        </LoginStatusContext.Provider>
    );
}

export default UserLoginContextProvider;