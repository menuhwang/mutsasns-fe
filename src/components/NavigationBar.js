import {Button, Container, Nav, Navbar} from "react-bootstrap";
import {useRecoilState} from "recoil";
import {tokenState} from "../recoil";
import {useContext, useEffect} from "react";
import {LoginStatusContext, LoginUserContext} from "./contexts/UserLoginContextProvider";
import {verify} from "../api/auth";

function NavigationBar() {
    const [token, setToken] = useRecoilState(tokenState);
    const {isLoggedIn, setLoggedIn} = useContext(LoginStatusContext);
    const {userData, setUserData} = useContext(LoginUserContext);

    useEffect(() => {
        if (token === '' || !token) {
            setLoggedIn(false);
        }else {
            fetch();
        }
        async function fetch() {
            verify(token).then((res) => {
                setUserData({
                    username: res.data.result.userName
                })
                setLoggedIn(true);
            })
        }
    }, [])

    return(
        <Navbar bg="warning" expand="lg">
            <Container>
                <Navbar.Brand href="/"><strong>MutsaSNS</strong></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        {/*로그인 여부 확인 후 로그인 버튼 또는 내정보 버튼, 로그아웃 버튼*/
                            isLoggedIn ?
                                <>
                                    <Button className="float-end me-lg-2 mt-2 mt-lg-0" variant="warning"><strong>{userData.username}</strong></Button>
                                    <Button className="float-end me-lg-2 mt-2 mt-lg-0" variant="light" onClick={() => {
                                        setToken('');
                                        window.location.href = "/";
                                    }}>로그아웃</Button>
                                </>
                                :
                                <>
                                    <Button className="float-end me-lg-2 mt-2 mt-lg-0" variant="light" href="/login">로그인</Button>
                                    <Button className="float-end me-lg-2 mt-2 mt-lg-0" variant="light" href="/join">회원가입</Button>
                                </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavigationBar;