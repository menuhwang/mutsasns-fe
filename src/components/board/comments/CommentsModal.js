import {useContext} from "react";
import {
    CommentDataContext,
    ModalsHandlerContext,
    ModalsStateContext
} from "../../contexts/CommentModalContextProvider";
import {Button, Modal} from "react-bootstrap";
import Comment from "./Comment";
import CommentNew from "./CommentNew";
import {LoginStatusContext} from "../../contexts/UserLoginContextProvider";

function CommentsModal(props) {
    const {isLoggedIn} = useContext(LoginStatusContext);
    const {comments} = useContext(CommentDataContext);
    const handler = useContext(ModalsHandlerContext);
    const state = useContext(ModalsStateContext);

    return (
        <Modal show={state} onHide={handler.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>댓글</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {isLoggedIn ? <CommentNew/> : <></>}
                {comments.length > 0 ? comments.map((comment) => <Comment key={comment.id} userName={comment.userName} comment={comment.comment}/>) : <>등록된 댓글이 없습니다.</>}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handler.handleClose}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default CommentsModal;