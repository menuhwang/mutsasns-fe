function Comment({userName, comment}) {
    return (
        <div>
            <strong className="me-2">{userName}</strong>
            {comment}
        </div>
    );
}

export default Comment;
