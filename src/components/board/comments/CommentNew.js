import {Button, Card, Form, InputGroup} from "react-bootstrap";
import React, {useContext, useState} from "react";
import {CommentDataContext, SelectedBoardContext} from "../../contexts/CommentModalContextProvider";
import {useRecoilState} from "recoil";
import {tokenState} from "../../../recoil";
import {getComments, writeComment} from "../../../api/comments";

function CommentNew() {
    const {selectedId} = useContext(SelectedBoardContext);
    const {setComments} = useContext(CommentDataContext);
    const [token, setToken] = useRecoilState(tokenState);

    const [body, setBody] = useState();
    return (
        <>
            <InputGroup className="mb-3">
                <Form.Control placeholder="댓글 작성" onChange={(e) => {setBody(e.target.value)}}/>
                <Button variant="warning" id="button-addon" onClick={() =>
                    writeComment(selectedId, body, token).then((res) => {
                        getComments(selectedId).then((res) => {
                            setComments(res.data.result.content);
                        })
                    }).catch((err) => alert(err.data.result.message))
                }>
                    <strong>등록</strong>
                </Button>
            </InputGroup>
        </>
    );
}

export default CommentNew;