import {Button, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import {getPosts} from "../../api/posts";
import {useRecoilState} from "recoil";
import {tokenState} from "../../recoil";
import PaginationBar from "./PaginationBar";

function BoardList(props) {
    const [paging, setPaging] = useState({});
    const [posts, setPosts] = useState([]);
    const [token, setToken] = useRecoilState(tokenState);

    useEffect( () => {
        if (posts.length === 0) {
            getPosts(props.page).then((res) => {
                setPaging({
                    totalPages : res.data.result.totalPages,
                    currentPage : res.data.result.number
                });
                setPosts(res.data.result.content);
            }).catch((err) => alert(err.data.result.message))
        }
    }, [])

    return (
        <>
            <Table hover>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">작성자</th>
                    <th scope="col">제목</th>
                </tr>
                </thead>
                <tbody>
                {posts.map((item) => (
                    <tr key={item.id}  onClick={() => {
                        document.location.href = `/boards?id=${item.id}`
                    }}>
                        <td>{item.id}</td>
                        <td>{item.userName}</td>
                        <td>{item.title}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
            <PaginationBar currentPage={paging.currentPage} lastPage={paging.totalPages}/>
            {token !== '' ? <Button variant="primary" href="/boards/new">글 작성</Button> : <></>}
        </>
    );
}

export default BoardList;