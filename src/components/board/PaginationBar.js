import {Pagination} from "react-bootstrap";

function PaginationBar(props) {
    return (
        <Pagination>
            <Pagination.Prev disabled={props.currentPage === 0} href={`/?page=${props.currentPage - 1}`}/>
            <Pagination.Next disabled={props.currentPage === props.lastPage - 1} href={`/?page=${props.currentPage + 1}`}/>
        </Pagination>
    );
}

export default PaginationBar;