import {useContext, useState} from "react";
import {Button, Card} from "react-bootstrap";
import {useRecoilState} from "recoil";
import {tokenState} from "../../recoil";
import {getPost, likes} from "../../api/posts";
import {CommentDataContext, ModalsHandlerContext, SelectedBoardContext} from "../contexts/CommentModalContextProvider";
import {getComments} from "../../api/comments";

function Board(props) {
    const [token, setToken] = useRecoilState(tokenState);
    const [board, setBoard] = useState(props.data);
    const {setSelectedId} = useContext(SelectedBoardContext);
    const {setComments} = useContext(CommentDataContext);
    const {handleShow} = useContext(ModalsHandlerContext);
    return (
        <>
            <Card className="mb-5 shadow bg-body rounded">
                <Card.Header><h5>{"@" + board.userName}</h5></Card.Header>
                <Card.Title><div className="ms-3 mt-3">{board.title}</div></Card.Title>
                <Card.Body>
                    <div className="pb-5">
                        <Card.Text>
                            {board.body}
                        </Card.Text>
                    </div>
                </Card.Body>
                <Card.Footer>
                    <Button variant="light" disabled={!token} onClick={() => {
                        likes(board.id, token).then((res) => {
                            alert(res.data.result);
                            getPost(board.id).then((res) => {
                                setBoard(res.data.result)
                            }).catch((err) => alert(err.data.result.message))
                        }).catch((err) => alert(err.data.result.message));
                    }}>👍 {board.likes}</Button>
                    <Button variant="light" onClick={() => {
                        getComments(board.id).then((res) => {
                            setSelectedId(board.id);
                            setComments(res.data.result.content);
                        })
                        handleShow();
                    }}>💬</Button>
                </Card.Footer>
            </Card>
        </>
    );
}

export default Board