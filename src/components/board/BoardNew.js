import {Button, Card, Form} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import {useRecoilState} from "recoil";
import {tokenState} from "../../recoil";
import {writePost} from "../../api/posts";

function BoardNew() {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [token, setToken] = useRecoilState(tokenState);

    return (
        <>
            <Card className="mb-5 shadow bg-body rounded">
                <Form>
                    <Card.Header>
                        <Form.Group controlId="formTitle">
                            <Form.Control type="text" placeholder="제목" onChange={(e) => {setTitle(e.target.value)}}/>
                        </Form.Group>
                    </Card.Header>
                    <Card.Body>
                        <Form.Group className="mb-3" controlId="formBody">
                            <Form.Control as="textarea" rows={5} placeholder="본문" onChange={(e) => {setBody(e.target.value)}}/>
                        </Form.Group>
                        <Button variant="warning" onClick={() =>
                            writePost(title, body, token).then((res) => {
                                alert(res.data.result.message);
                                window.location.href = "/";
                            }).catch((err) => alert(err.data.result.message))
                        }>
                            작성
                        </Button>
                    </Card.Body>
                </Form>
            </Card>
        </>
    );
}

export default BoardNew;